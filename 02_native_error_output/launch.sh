#!/usr/bin/env bash

case $1 in
normal*)
	sed -i 's/panic =/#panic =/' Cargo.toml
	cargo build
	cargo run
	;;
abort*)
	sed -i 's/#//g' Cargo.toml
	cargo build
	cargo run
	;;
trace*)
	cargo build
	RUST_BACKTRACE=1 cargo run
	;;
*)
	echo "use $0 <normal|abort|trace>"
	;;
esac
