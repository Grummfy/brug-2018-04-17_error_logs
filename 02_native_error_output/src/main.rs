#![allow(unused)]
fn main() {
    use std::error::Error;

    match "xc".parse::<u32>() {
        Err(e) => {
            println!("1. Error parsing some value: {}", e.description());
            eprintln!("2. Error parsing some value: {}", e.description());
        }
        _ => println!("No error"),
    }
}

// run normal
// run with redirection of error to something else
// cargo run
// cargo run 2> /dev/null
