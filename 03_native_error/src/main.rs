extern crate rand;

use std::io;
use std::error::Error;
use std::fmt;
use std::cmp::Ordering;
use rand::Rng;
use std::num::ParseIntError;

pub struct Guess {
    value: u32,
}

//
// create a custom error for boundaries
//

#[derive(Debug)]
pub struct NumberBoundariesError {
    value: u32,
}

impl Error for NumberBoundariesError {
    fn description(&self) -> &str {
        "The number to guess should be between 1 and 100"
    }
}

impl fmt::Display for NumberBoundariesError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{} is not a good number", self.value)
    }
}

//
// Create a custom error for parsing error
//

#[derive(Debug)]
pub struct BadInputError {
    value: String,
}

impl Error for BadInputError {
    fn description(&self) -> &str {
        "You need to enter a number nothing else!"
    }
}

impl fmt::Display for BadInputError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "'{}' occurs because you didn't enter a number", self.value)
    }
}

// make ParseIntError a BadInputError
// rust will convert it automatically
impl From<ParseIntError> for BadInputError {
    fn from(err: ParseIntError) -> BadInputError {
        BadInputError{value: err.description().to_string()}
    }
}

// implements our struct with the "business rules"
impl Guess {
    pub fn new(val: u32) -> Result<Guess, NumberBoundariesError> {
        if val < 1 || val > 100 {
            // println!("Guess value must be between 1 and 100, got {}", value);
            return Err(NumberBoundariesError { value: val });
        }

        Ok(Guess{value: val})
    }

    pub fn value(&self) -> u32 {
        self.value
    }

    pub fn cmp(&self, secret: &u32) -> Ordering {
        return self.value().cmp(secret);
    }
}

fn read_number() -> Result<u32, BadInputError> {
    let mut guess = String::new();
    io::stdin().read_line(&mut guess).expect("Failed to read line");

    let number: u32 = guess.trim().parse()?;
    Ok(number)
}

fn main() {
    println!("Guess the number! (between 1 and 100!)");

    let secret_number = rand::thread_rng().gen_range(1, 101);

    loop {
        println!("Please input your guess.");

        let guess = match read_number() {
            Ok(number) => match Guess::new(number) {
				Ok(guessing) => guessing,
				Err(error) => {
					eprintln!("Error (debug): {:?}", error); // debug mode
					eprintln!("Error (display): {}", error); // display mode
                    // see https://doc.rust-lang.org/std/fmt/#fmtdisplay-vs-fmtdebug
					continue;
				}
			}
            Err(error) => {
                eprintln!("Error (debug): {:?}", error); // debug mode
                eprintln!("Error (display): {}", error); // display mode
                continue;
            }
        };

        println!("You guessed: {}", guess.value());

        match guess.cmp(&secret_number) {
            Ordering::Less => println!("Too small!"),
            Ordering::Greater => println!("Too big!"),
            Ordering::Equal => {
                println!("You win!");
                break;
            }
        }
    }
}
