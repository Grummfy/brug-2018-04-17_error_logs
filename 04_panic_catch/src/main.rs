use std::panic;

fn main() {
    let result = panic::catch_unwind(|| {
        panic!("crash and burn");
    });

    println!("{:?}", result.is_err());
    println!("{:?}", result);
}
