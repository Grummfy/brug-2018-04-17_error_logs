#[macro_use]
extern crate human_panic;

fn main() {
    setup_panic!();

    panic!("something went wrong");
}
