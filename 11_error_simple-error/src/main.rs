#[macro_use]
extern crate simple_error;

use std::error::Error;

fn demo(value: &str) -> Result<u32, Box<Error>> {
    let x: u32 = value.parse()?;
    if x > 100 {
        bail!(format!("The value '{}' is too high", x))
    }

    Ok(x)
}

fn main() {
    println!("{:?}", demo("x"));  // Err(ParseIntError { kind: InvalidDigit })
    println!("{:?}", demo("101")); // Err(StringError("The value \'101\' is too high"))
    println!("{:?}", demo("10")); // Ok(10)
}
