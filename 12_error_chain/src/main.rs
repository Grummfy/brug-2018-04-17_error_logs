#[macro_use]
extern crate error_chain;

use error_chain::ChainedError;

// Create the Error, ErrorKind, ResultExt, and Result types
error_chain! {
    types {
        Error, ErrorKind, ResultExt, Result;
    }

    foreign_links {
        Std(::std::num::ParseIntError);
    }

    errors {
        BadInput(value: String) {
            description("bad input, we required a number"),
            display("This '{}' is not a valid number", value),
        }
        TooBig(value: u32) {
            description("The value is too high"),
            display("This '{}' is not a valid number", value),
        }
        CustomOne
    }
}

fn some_error() -> Result<()> {
    Err(ErrorKind::CustomOne.into())
}

fn demo(value: &str) -> Result<u32> {
    let x: u32 = value.parse()
        .chain_err(|| "use a number!") // we chain an error in case of error
        .chain_err(|| ErrorKind::BadInput(value.to_string()))?; // chain a custom one
    if x > 100 {
        // bail!(format!("The value '{}' is too high", x))
        bail!(ErrorKind::TooBig(x))
    }

    Ok(x)
}

fn main() {
    println!("{:?}", some_error());
    println!("{:?}", demo("x"));
    match demo("x") {
        Ok(_) => println!("OK"),
        Err(e) => {
            println!("{}", e.display_chain().to_string());
            if let Some(backtrace) = e.backtrace() {
                // use RUST_BACKTRACE=1
                println!("backtrace: {:?}", backtrace);
            }
        }
    }
    println!("{:?}", demo("101"));
    println!("{:?}", demo("10"));
}
