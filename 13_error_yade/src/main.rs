#[macro_use]
extern crate yade;

#[derive(Debug, YadeError)]
pub enum MyError {
    #[display(msg = "The value '{}' is not a number", _0)]
    BadInput(String, #[cause] std::num::ParseIntError),
}

fn demo(value: &str) -> Result<u32, MyError> {
    let x: u32 = value.parse()
        .map_err(|e| MyError::BadInput(value.to_string(), e))?;

    Ok(x)
}

fn main() {
    println!("{:?}", demo("x"));
    println!("{:?}", demo("10"));
}
