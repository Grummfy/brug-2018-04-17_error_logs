#[macro_use]
extern crate log;
extern crate env_logger;

use log::Level;

// RUST_LOG=trace cargo run
// RUST_LOG=log_log=trace cargo run
// RUST_LOG=log_log=warn cargo run

fn main() {
    env_logger::init();

    trace!("Starting the demo");
    println!("Hello world");
    info!("print info log");
    debug!("print debug log");
    warn!("print warn log");

    if log_enabled!(Level::Trace) {
        // do some debug info
        trace!("tracing info {}", 42);
    }
}
