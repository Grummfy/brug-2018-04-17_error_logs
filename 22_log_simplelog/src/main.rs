#[macro_use]
extern crate log;
extern crate simplelog;

use simplelog::*;
use std::fs::File;

fn main() {
    CombinedLogger::init(
        vec![
            TermLogger::new(LevelFilter::Warn, Config::default()).unwrap(),
            WriteLogger::new(LevelFilter::Info, Config::default(), File::create("my_rust_binary.log").unwrap()),
        ]
    ).unwrap();

    trace!("Starting the demo");
    println!("Hello world");
    info!("print info log");
    debug!("print debug log");
    warn!("print warn log");
}
