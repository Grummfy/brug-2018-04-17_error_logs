#[macro_use]
extern crate log;
extern crate log4rs;

fn main() {
    log4rs::init_file("log4rs.yaml", Default::default()).unwrap();

    trace!("Starting the demo");
    println!("Hello world");
    info!("print info log");
    debug!("print debug log");

    foo::bar();

    warn!("print warn log");
}

mod foo {
    pub fn bar() {
        warn!("bar");
    }
}