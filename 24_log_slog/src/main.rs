#[macro_use]
extern crate slog;
extern crate slog_json;
extern crate slog_term;

use slog::Drain;
use std::io;
use std::sync::Mutex;

fn main() {
    let plain = slog_term::PlainSyncDecorator::new(std::io::stdout());
    // we create 2 output for the log term and a formater to stderr
    let d1 = slog_term::FullFormat::new(plain).build().fuse();
    let d2 = Mutex::new(slog_json::Json::default(io::stderr())).fuse();

    let log = slog::Logger::root(slog::Duplicate::new(d1, d2).fuse(), o!(
        "version" => env!("CARGO_PKG_VERSION"),
        "yo" => "lo")
    );

    trace!(log, "Starting the demo");

    println!("Hello world");

    info!(log, "print info log");
    debug!(log, "print debug log");
    warn!(log, "print warn log");
}
