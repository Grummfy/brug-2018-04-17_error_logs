#[macro_use]
extern crate slog;
extern crate sloggers;

use sloggers::Build;
use sloggers::terminal::{TerminalLoggerBuilder, Destination};
use sloggers::types::Severity;

fn main() {
    let mut builder = TerminalLoggerBuilder::new();
    builder.level(Severity::Info);
    builder.destination(Destination::Stderr);

    let logger = builder.build().unwrap();

    trace!(logger, "Starting the demo");

    println!("Hello world");

    info!(logger, "print info log");
    debug!(logger, "print debug log");
    warn!(logger, "print warn log");

}
